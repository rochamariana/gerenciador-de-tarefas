import cors from 'cors';
import express, { } from 'express';
import tipos from './tipos';
import tarefas from './tarefas';

const router = express.Router();

router.use(cors());

router.use(tarefas);
router.use(tipos);

export default router;