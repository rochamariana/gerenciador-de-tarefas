import express, {Express, Request, Response, NextFunction} from 'express';
import Router from 'express'
const router = Router();
import Tarefa from '../models/Tarefas';
import TarefasController from '../controllers/TarefasController';

const validadeTarefaId = async (req: Request, res: Response, next: NextFunction) => {
  const tarefa = await Tarefa.findByPk(req.params.tarefaId);
  if (!tarefa) {
    return res.status(404).json({ error: 'Tarefa não encontrada!' });
  }
  next();
}

router.get('/tarefas', TarefasController.index);

router.post('/tarefas', TarefasController.create);

router.put('/tarefas/:tarefaId', validadeTarefaId, TarefasController.update);

router.delete('/tarefas/:tarefaId', validadeTarefaId, TarefasController.delete); 

router.get('/email/:email', TarefasController._sendMail);

export default router;