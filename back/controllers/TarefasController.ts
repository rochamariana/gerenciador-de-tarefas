import { Op } from 'sequelize';
import Tarefa from '../models/Tarefas';
import Tipo from '../models/Tipos';
import nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
 
import express, { Express, NextFunction, Request, response, Response } from 'express';

export class TarefasController {

  index = async (req: Request, res: Response) => {
    const params: any= req.query;
    const sort: any = params.sort || 'prioridade';
    const order: any = params.order || 'ASC';
    const where: any = {};

    if(params.TipoId)
    {
      where.TipoId = 
      {
        [Op.eq]: params.TipoId
      }
    }
  
    const tarefa = await Tarefa.findAll({
  
      include:[{
        model: Tipo,
        required: false,
        attributes: ['descricao']
      }], 
      where: where,
      order: [ [sort, order] ]
    });
    res.json(tarefa);
  }

  create = async (req: Request, res: Response) => {
    try {
      const data: any = await this._validateData(req.body, req.params.tarefaId );
      const tarefa = await Tarefa.create(data);
      res.json(tarefa);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  }

  show = async (req: Request, res: Response) => {
    const tarefa = await Tarefa.findByPk(req.params.tarefaId);
    res.json(tarefa);
  }

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id : any = req.params.tarefaId;
      const data = await this._validateData(req.body, id);
      await Tarefa.update(data, {
        where: {
          id: id
        }
      });

      res.json(await Tarefa.findByPk(id));
    } catch (error) {
      res.status(400).json({error});
    }
  }

  delete = async (req: Request, res: Response, next: NextFunction) => {
    await Tarefa.destroy({
      where: {
        id: req.params.tarefaId
      }
    });
    res.json({});
  }


  _validateData = async (data: any, id?: any) => {
    const attributes = ['data_criacao', 'data_vencimento', 'descricao', 'situacao', 'prioridade', 'TipoId'];
    const tarefa : any = {};
    for (const attribute of attributes) {
      if (! data[attribute]){
        throw (`The attribute "${attribute}" is required.`);
      }
      tarefa[attribute] = data[attribute];
    }
    return tarefa;
  }

  _sendMail = async (req: Request, res: Response, next: NextFunction,) => {

    const data : any = await Tarefa.findByPk(req.params.tarefaId);
   
    let email_user = 'luis.kochenborger@universo.univates.br';
    let email_password = 'Xjk011-2';
    let email_to = req.params.email;
    let email_subject = 'Nova tarefa adicionada!';
    let email_content = `Você tem uma nova tarefa!

    Data Criação: ${data.data_criacao}
    Data Vencimento: ${data.data_vencimento}
    Tarefa: ${data.descricao}
    Situação: ${data.situacao}
    Prioridade: ${data.prioridade}
    Tipo: ${data.TipoId}
    `;

    var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: email_user,
        pass: email_password
      }
    });

    var mailOptions = {
      from: email_user,
      to: email_to,
      subject: email_subject,
      text: email_content
    }

    transporter.sendMail(mailOptions, (error: Error | null, info: SMTPTransport.SentMessageInfo) => {
      if (error) {
        console.log('Erro on sendMail:' + error);
      } else {
        console.log('Mail sent!');
      }
    });
  }


}

export default new TarefasController();
