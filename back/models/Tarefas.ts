import { DataTypes, Model } from 'sequelize';
import db from '../db';
import Tipo from './Tipos';


class Tarefa extends Model {
  declare id: number;
  declare data_criacao: Date;
  declare data_vencimento: Date;
  declare descricao: string;
  declare situacao: string;
  declare prioridade: number;
 };

Tarefa.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  data_criacao: {
    type: DataTypes.DATE,
    allowNull: false
  },
  data_vencimento: {
    type: DataTypes.DATE,
    allowNull: false
  },
  descricao: {
    type: DataTypes.STRING,
    allowNull: false
  },
  situacao: {
    type: DataTypes.STRING,
    allowNull: false
  },
  prioridade: {
    type: DataTypes.INTEGER,
  }, 
}, {
    sequelize: db,
    tableName: 'tarefas',
    modelName: 'Tarefa'
});

Tipo.hasMany(Tarefa)
Tarefa.belongsTo(Tipo);

export default Tarefa;
